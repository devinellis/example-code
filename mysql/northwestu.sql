/**
 * Devin Ellis Sample Code
 * Example MySQL Queries from northwestu.edu
 * Note: These are rather old queries, I prefer using abstraction layers now.
 * 2004 ~ 2010
 */


SELECT	LEFT( event_id, 4 ) AS 'department'
,		RIGHT( event_id, 4) AS 'number'
,		CASE
        WHEN publication_name_1 IS NOT NULL THEN publication_name_1 + ISNULL(publication_name_2, '')
        WHEN event_long_name IS NOT NULL THEN event_long_name
        ELSE ISNULL(event_med_name, '')
        END AS 'title'
,		ISNULL( description, '' ) AS 'description'
FROM	event
WHERE	event_status = 'A'
AND		event_id = '$this->event_id';


SELECT		s.academic_term as 'term'
,			COUNT( s.academic_term ) AS 'sections'
FROM		event e
JOIN		sections s
ON			s.event_id = e.event_id
WHERE		e.event_status = 'A'
AND			e.event_id = '$this->event_id'
AND			YEAR(s.start_date) >= (YEAR(GETDATE())-2)
GROUP BY	s.academic_term
HAVING		COUNT( s.academic_term ) >= 2;


SELECT		g.filename
,			g.number
,			g.GID
,			gl.slug
,			gl.name
FROM		galleries g
JOIN		gallerylist gl ON g.gallery = gl.id
WHERE		gallery = (
					   SELECT		gallery
					   FROM			galleries
					   WHERE		GID = $GID
					   LIMIT		1
					   )
ORDER BY	number ASC;
