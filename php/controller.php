<?php
/**
 * Devin Ellis Sample Code
 * Example Controller from 3do3dont.com
 * This is production code, no changes have been made for this portfolio.
 * 9/11/2011
 */
class IndexController extends DDD_Controller
{
	
	/**
	 * Homepage
	 */
	public function IndexAction()
	{
		$movies = new DDD_Movies();
		$movies->fetchCurrent();
		$this->view->set('movies', $movies->collection());
		$this->view->set('pageTitle', '3D Movie Reviews - Are 3D versions better than 2D? | 3do3dont');
	}
	
	/**
	 * Movie Detail Page
	 */
	public function MovieAction($void, $urlSegment = false)
	{
		
		// if no urlSegment, redirect to homepage
		if(!$urlSegment) {
			$urlSegment = $void;
		}
		if(!$urlSegment) {
			$this->redirect('/');
		}
		
		// 301 crap links
		if($this->_request->uri() != 'm/' . $urlSegment) {
			header('HTTP/1.1 301 Moved Permanently');
			$this->redirect('/m/' . $urlSegment);
		}
		
		// Load movie data
		$movies = new DDD_Movies();
		$movies->whereEqual('urlSegment', $urlSegment)->fetchOne();
		$this->view->set('movie', $movies);
		
		// Load trailer
		$trailers = new DDD_Trailers();
		$trailers->whereEqual('movieID', $movies->movieID)->fetchOne();
		if($trailers->trailerID > 0) {
			$this->view->set('trailer', $trailers);
		}
		
		// Load poster
		$images = new DDD_Images();
		$images->whereEqual('imageID', $movies->posterID)->fetchOne();
		$this->view->set('poster', $images);
		
		// Load banner
		$images->whereEqual('imageID', $movies->bannerID)->fetchOne();
		$this->view->set('banner', $images);
		
		// Load tweets
		$twitter = new DDD_API_Twitter();
		$twitter->limit(20);
		#$positive = $twitter->positive($movies->shortName);
		#$negative = $twitter->negative($movies->shortName);
		#$this->view->set('positiveTweets', $positive);
		#$this->view->set('negativeTweets', $negative);
		$tweets = $twitter->all($movies->shortName);
		$this->view->set('tweets', $tweets);
		
		
		// Load criteria
		$criteria = new DDD_MovieCriterias();
		$criteria = $criteria->getByMovie($movies->movieID);
		$this->view->set('criterias', $criteria);

		// Load versions
		$versions = new DDD_MovieVersions();
		$versions = $versions->getByMovie($movies->movieID);
		$this->view->set('versions', $versions);

		// Load reviews
		$reviews = new DDD_Reviews();
		$this->view->set('totalReviews', $reviews->countByMovie($movies->movieID));
		$reviews = $reviews->getByMovie($movies->movieID, 3);
		$this->view->set('reviews', $reviews);
		$this->view->set('pageTitle', $movies->shortName . ' 3D Movie Review - Is it worth watching in 3D? | 3do3dont');

	}
	
	/**
	 * All Movie Reviews
	 */
	public function ReviewsAction($urlSegment)
	{
		// if no urlSegment, redirect to homepage
		if(!$urlSegment) {
			$this->redirect('/');
		}
		
		// Load movie data
		$movies = new DDD_Movies();
		$movies->whereEqual('urlSegment', $urlSegment)->fetchOne();
		$this->view->set('movie', $movies);
		
		// Load reviews
		$reviews = new DDD_Reviews();
		$reviews = $reviews->getByMovie($movies->movieID);
		$this->view->set('reviews', $reviews);
		$this->view->set('pageTitle', $movies->shortName . ' 3D Movie Review - Should I see it in 3D? | 3do3dont');

	}
}