<?php
/**
 * Devin Ellis Sample Code
 * Custom JSON API for 3do3dont.com
 * This is production code, no changes have been made for this portfolio.
 * 9/11/2011
 */
class ReviewsController extends DDD_Controller
{
	public function preDispatch()
	{
		$this->view->setLayout('json', false);
	}

	public function IndexAction()
	{
	}

	/**
	 * Returns an empty review form
	 */
	public function FormAction()
	{
		$status 	= 'ok';
		$response 	= 'success';
		$this->view->set('movieID', $this->post('movieID'));
		$data 		= $this->view->renderElement('../../default/elements/reviewForm');

		$this->view->set('status', $status);
		$this->view->set('response', $response);
		$this->view->set('data', $data);
	}

	/**
	 * Create a review
	 */
	public function CreateAction()
	{
		try
		{
			$movieID = (int) $this->post('movieID');
			// Make sure movieID exists
			if($movieID == 0) {
				throw new Exception('Invalid movieID');
			}

			$reviews = new DDD_Reviews();

			// make sure no one from this IP has reviewed this movie yet
			$reviews->whereEqual('ip', $_SERVER['REMOTE_ADDR'])
				->whereEqual('movieID', $movieID)
				->fetchOne();
			if($reviews->movieID > 0) {
				throw new Exception('You have already reviewed this movie');
			}

			$reviewForm = new DDD_Forms_Review();
			$reviewForm->validate();
			$reviews->create()
				->setData($reviewForm->getData())
				->save();

			$reviews->selectVoteID()->fetchOne($reviews->reviewID);

			$this->view->set('reviews', array($reviews));
			$data 		= $this->view->renderElement('../../default/elements/review');
			$status 	= 'ok';
			$response 	= 'success';

			// send notification email
			try {
				$email = new DDD_Email();

				$email->setSubject('Review Submitted');
				$body = $reviewForm->renderText();
				$email->addTo('email@example.com');
				$email->setBodyHtml($body);
				$email->send();
			} catch (Exception $e) {
			}

		} catch(Exception $e)
		{
			$status 	= 'ok';
			$response 	= $e->getMessage();
			$data 		= $this->post();
		}
		$this->view->set('status', $status);
		$this->view->set('response', $response);
		$this->view->set('data', $data);

		if ($this->_request->isAjax() == false) {
			$this->redirect('/');
		}
	}

	/**
	 * Vote up/down a review
	 */
	public function VoteAction()
	{
		try
		{
			// Make sure movieID exists
			$reviewID = (int) $this->post('reviewID');
			if($reviewID == 0) {
				throw new Exception('Invalid reviewID');
			}

			$reviews = new DDD_Reviews();
			$votes = new DDD_Votes();

			// Get the review
			$reviews->selectVoteID()->fetchOne($this->post('reviewID'));

			if(empty($reviews->reviewID)) {
				throw new Exception('Invalid reviewID');
			}

			// make sure no one from this IP has voted on this review yet
			$votes->whereEqual('ip', $_SERVER['REMOTE_ADDR'])
				->whereEqual('reviewID', $reviewID)
				->fetchOne();
			if($votes->voteID > 0) {
				throw new Exception('You have already voted on this review');
			}

			// Create the vote record
			$votes->create()
				->set('reviewID', $this->post('reviewID'))
				->set('vote', $this->post('vote'))
				->save();

			// Add the vote to the review's total, refresh reviews object
			$reviews->vote($votes->vote);
			$reviews->selectVoteID()->fetchOne($this->post('reviewID'));

			// Get html render
			$this->view->set('reviews', array($reviews));
			$html = $this->view->renderElement('../../default/elements/review');

			$status 	= 'ok';
			$response 	= 'success';
			$data = array(
				'reviewID'	=> $reviews->reviewID,
				'movieID'	=> $reviews->movieID,
				'name'		=> $reviews->name,
				'verdict'	=> $reviews->verdict,
				'comment'	=> $reviews->comment,
				'score'		=> $reviews->score,
				'tsCreated'	=> $reviews->tsCreated,
				'avatar'	=> $reviews->gravatar(),
				'html'		=> $html,
			);
		} catch(Exception $e)
		{
			$status 	= 'ok';
			$response 	= $e->getMessage();
			$data 		= array();
		}
		$this->view->set('status', $status);
		$this->view->set('response', $response);
		$this->view->set('data', $data);

		if ($this->_request->isAjax() == false) {
			$this->redirect('/');
		}
	}

	/**
	 * Delete a user's vote on a review
	 */
	public function UndovoteAction()
	{
		try
		{
			// Make sure movieID exists
			$reviewID = (int) $this->post('reviewID');
			if($reviewID == 0) {
				throw new Exception('Invalid reviewID');
			}

			$reviews = new DDD_Reviews();
			$votes = new DDD_Votes();

			// Get the review
			$reviews->selectVoteID()->fetchOne($this->post('reviewID'));

			if(empty($reviews->reviewID)) {
				throw new Exception('Invalid reviewID');
			}

			// make sure no one from this IP has voted on this review yet
			$votes->whereEqual('ip', $_SERVER['REMOTE_ADDR'])
				->whereEqual('reviewID', $reviewID)
				->fetchOne();
			if($votes->voteID == 0) {
				throw new Exception('You have not voted on this review');
			}

			// Delete the vote record, refresh the reviews object
			$votes->delete();
			$reviews->selectVoteID()->fetchOne($this->post('reviewID'));

			// remove vote from review's total
			$vote = ($votes->vote == 1) ? -1 : 1;
			$reviews->vote($vote);

			// Get html render
			$this->view->set('reviews', array($reviews));
			$html = $this->view->renderElement('../../default/elements/review');

			$status 	= 'ok';
			$response 	= 'success';
			$data = array(
				'reviewID'	=> $reviews->reviewID,
				'movieID'	=> $reviews->movieID,
				'name'		=> $reviews->name,
				'verdict'	=> $reviews->verdict,
				'comment'	=> $reviews->comment,
				'score'		=> $reviews->score,
				'tsCreated'	=> $reviews->tsCreated,
				'avatar'	=> $reviews->gravatar(),
				'html'		=> $html,
			);
		} catch(Exception $e)
		{
			$status 	= 'ok';
			$response 	= $e->getMessage();
			$data 		= array();
		}
		$this->view->set('status', $status);
		$this->view->set('response', $response);
		$this->view->set('data', $data);

		if ($this->_request->isAjax() == false) {
			$this->redirect('/');
		}
	}
}
